package com.simion;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Simion on 08/03/2017.
 */
public interface OpinionRepository extends CrudRepository<Opinion, Integer> {

    List<Opinion> findAll();
    List<Opinion> findByPuntuacion(int puntuacion);
}