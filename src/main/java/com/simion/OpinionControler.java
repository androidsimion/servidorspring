package com.simion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * Created by Simion on 08/03/2017.
 */

@RestController
public class OpinionControler {

    @Autowired
    private OpinionRepository repository;

    /**
     * Obtiene todas las opiniones de los usuarios
     * @return
     */
    @RequestMapping("/opiniones")
    public List<Opinion> getOpiniones() {

        List<Opinion> listaOpiniones = repository.findAll();
        return listaOpiniones;
    }

    /**
     * Obtiene todas las opiniones con una puntuacion determinada
     * @param puntuacion
     * @return
     */
    @RequestMapping("/opiniones_puntuacion")
    public List<Opinion> getOpiniones(int puntuacion) {

        List<Opinion> listaOpiniones = repository.findByPuntuacion(puntuacion);
        return listaOpiniones;
    }

    /**
     * Registra una nueva opinión en la Base de Datos
     * @param nombre
     * @param comentario
     * @param puntuacion
     */
    @RequestMapping("/add_opinion")
    public void addOpinion(
            @RequestParam(value = "nombre", defaultValue = "nada") String nombre,
            @RequestParam(value = "comentario" , defaultValue = "nada mas") String comentario,
            @RequestParam(value = "puntuacion", defaultValue = "-1") int puntuacion) {

        Opinion opinion = new Opinion();
        opinion.setNombre(nombre);
        opinion.setComentario(comentario);
        opinion.setFecha(new Date(System.currentTimeMillis()));
        opinion.setPuntuacion(puntuacion);

        repository.save(opinion);
    }
}